// prettier.config.js or
module.exports = {
  printWidth: 100,
  tabWidth: 2,
  trailingComma: 'es5',
  bracketSpacing: true,
  singleQuote: true,
};