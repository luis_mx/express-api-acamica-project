# Sprint Project API&ensp;🚀

Express project for Acamica's BackEnd Developer Course — Sprint 2.

## Installation

On project's root folder install dependancies by:

```
npm install
```

After installing dependancies run

```
npm start
```

## Important DB & REDIS

This project is consuming a **MongoDB** Storage & **Redis** Storage on the _Cloud_. All connection configurations are held in the file `./.env`.

## DB & Redis Configuration

Project env configuration: `./server/config/env.js`

When there is no `.env` file present, all variables have a fallback. Adjust any configuration neede to run in your environment. 💻

## Seeding DB Script

This script will populate the DB with content for:

- Users
- Products
- Payments

```
npm run seed
```

**Warning**: This script will override any content stored in DB.

## Testing _/signup_ :

```
npm test
```

## Retrieving Prodcuts from Cache (Redis)

GET Access to `/products` has a middleware called `getAllProductsFromCache.js`
which first access Redis and scans for product keys stored.

There will be a log message informing whether product information was retrieved from **Redis** or from **Databse**.

Same goes for GET `products/{id}`.

PUT `products/{id}` updates Redis key information `product:{id}`.

DELETE `products/{id}` unlink Redis key `product:{id}`.

## Using Swagger Docs

Swagger documentation will be listed on:

```
http://127.0.0.1:{port}/{vAPI}/api-docs
```

All endpoints, except for Login & Sign Up, require an Authorization Token.

To do so

- Click on <strong>/login</strong> endpoint.
- From the dropdown, it's possible to choose between <strong>Admin</strong> and <strong>Regular</strong> user.
- Click <strong>Try it out</strong> and <strong>Execute</strong>.
- Response should bring back a <strong>Token</strong> string 🎟️.
- Click <strong>Authorize</strong> button and paste token.
- Lock icon should look close now 🔒.

```
For Admin:
{
  email: admin@email.com
  password: admin
}
```

```
For Basic User:
{
  email: basico@email.com
  password: lalala
}
```

👆 This users will be included in DB when running `npm run seed` script.
