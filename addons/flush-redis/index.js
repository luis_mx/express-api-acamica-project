const { redisUnlink } = require('../../server/config/redis');
const log = require('../../server/config/logger');
const { retrieveProductKeysFromCache, Timer } = require('../../utils');

const timer = new Timer('deleting product keys from redis');

(async function redisUnlinkAllProducts() {
  try {
    const productKeys = await retrieveProductKeysFromCache();
    timer.start();
    productKeys.map(async (productKey) => await redisUnlink(productKey));
    timer.end();
    process.exit(0);
  } catch (error) {
    log.fatal(error.stack);
    process.exit(1);
  }
})();
