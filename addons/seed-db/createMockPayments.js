const { Payment } = require('../../models');
const log = require('../../server/config/logger');
const { Timer } = require('../../utils');

const timer = new Timer('seeding payments');

module.exports = async function createMockPayments() {
  timer.start();
  try {
    await Payment.deleteMany();
    const payments = retrievePayments().map(({ type, name }) => new Payment({ type, name }));
    await Payment.create(payments);
    timer.end();
  } catch (error) {
    log.fatal(error.stack);
  }
};

function retrievePayments() {
  return [
    {
      type: 'CASH',
      name: 'Efectivo',
    },
    {
      type: 'CARD',
      name: 'Tarjeta',
    },
    {
      type: 'Paypal',
      name: 'Paypal',
    },
    {
      type: 'BTC',
      name: 'Bitcoin',
    },
  ];
}
