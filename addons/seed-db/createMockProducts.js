const { Product } = require('../../models');
const log = require('../../server/config/logger');
const { randomNumberRange, Timer } = require('../../utils');

const timer = new Timer('seeding products');

module.exports = async function createMockProducts() {
  timer.start();
  try {
    await Product.deleteMany();

    const products = [
      new Product({
        _id: '6132e81ad91dae30eecb2ce8',
        name: 'Hamburger',
        price: parseInt(`${randomNumberRange(60, 380)}00`),
      }),
      new Product({
        _id: '6132e81ad91dae30eecb2ce9',
        name: 'Cheesecake',
        price: parseInt(`${randomNumberRange(60, 380)}00`),
      }),
      ...retrieveDishes().map(
        (dish) =>
          new Product({
            name: dish,
            price: parseInt(`${randomNumberRange(60, 380)}00`),
          })
      ),
    ];

    // create 1000 products
    // const products = new Array(10)
    //   .fill(null)
    //   .reduce((arr, _) => arr.concat(runArrayProducts()), []);

    await Product.create(products);
    timer.end();
  } catch (error) {
    log.fatal(error.stack);
  }
};

function runArrayProducts() {
  return retrieveDishes().map(
    (dish) =>
      new Product({
        name: `${dish} ${randomNumberRange(60, 993475987)}`,
        price: parseInt(`${randomNumberRange(60, 380)}00`),
      })
  );
}

function retrieveDishes() {
  return [
    'Minestrone',
    'Baked Alaska',
    "General Tso's chicken",
    'Chicken soup',
    'Laksa',
    'Hot and sour soup',
    'Sundae',
    'Apple pie',
    'Trifle',
    'Peach Melba',
    'Steak tartare',
    'Beef Wellington',
    'Steak and kidney pie',
    'Bacon and egg pie',
    'Corned beef pie',
    'Kalakukko',
    'Meat and potato pie',
    'Pasty',
    'Pork pie',
    'Pot pie',
    'Quiche',
    'Scotch pie',
    'Cottage pie',
    'Stargazy pie',
    'Steak pie',
    'Sweet potato pie',
    'Banoffee pie',
    'Banana cream pie',
    'Blackberry pie',
    'Blueberry pie',
    'Boston cream pie',
    'Buko pie',
    'Cherry pie',
    'Chess pie',
    'Cream pie',
    'Custard pie',
    'Dutch apple pie',
    'Key lime pie',
    'Lemon meringue pie',
    'Mince pie',
    'Pecan pie',
    'Pumpkin pie',
    'Rhubarb pie',
    'Strawberry pie',
    'Sugar pie',
    'Mie goreng',
    'Nasi goreng',
    'Dal',
    'Irish stew',
    'Cochinita pibil',
    'Torta',
    'Gordita',
    'Burrito',
    'Chocolate brownie',
    'Sinseollo',
    'Pad Thai',
    'Kow Pad Gai',
    'Rat na',
    'Phat si-io',
    'Drunken noodles',
    'Khao soi',
    'Tom yum',
    'Phat khing',
    'Tom kha kai',
    'Red curry',
    'Green curry',
    'Massaman curry',
    'Biryani',
    'Pilaf',
    'Risotto',
    'Wedding soup',
    'Green papaya salad',
    'Larb',
    'Kai yang',
    'Lemang',
    'Sushi',
    'Sashimi',
    'Okonomiyaki',
    'Bulgogi',
    'Galbi',
    'Jokbal',
    'Samgyeopsal',
    'Hoe',
    'Sannakji',
    'Makchang gui',
    'Gopchang',
    'Gujeolpan',
    'Vindaloo',
    'Baked ziti',
    'Tandoori chicken',
    'Butter chicken',
    'Palak paneer',
    'Mixed grill',
    'Eggs Benedict',
    'Scrambled eggs',
    'Tabbouleh',
    'Caesar salad',
    'Waldorf salad',
  ];
}
