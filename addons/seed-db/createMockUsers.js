const faker = require('faker/locale/es_MX');
// const bcrypt = require('bcryptjs');

const { User } = require('../../models');
const log = require('../../server/config/logger');
const { Timer } = require('../../utils');

const timer = new Timer('seeding users');

module.exports = async function createMockUsers() {
  timer.start();
  try {
    await User.deleteMany();

    const users = [superAdmin(), basicUser(), ...arrayOfRandomUsers()];

    await User.create(users);
    timer.end();
  } catch (error) {
    log.fatal(error.stack);
  }
};

function superAdmin() {
  const admin = new User({
    isAdmin: true,
    email: 'admin@email.com',
    password: 'admin',
    username: 'admin',
    addressBook: [{ destination: faker.address.streetAddress(), alias: 'default' }],
  });
  admin.generateAuthToken();
  return admin;
}

function basicUser() {
  const basic = new User({
    _id: '6132e818d91dae30eecb2cbd',
    email: 'basico@email.com',
    password: 'lalala',
    username: 'basico',
    addressBook: [{ destination: faker.address.streetAddress(), alias: 'default' }],
  });
  basic.generateAuthToken();
  return basic;
}

function arrayOfRandomUsers() {
  return Array(9)
    .fill(null)
    .map((_) => {
      const user = new User({
        email: faker.internet.email(),
        password: 'lalala',
        username: faker.internet.userName(),
        fullName: `${faker.name.firstName()} ${faker.name.lastName()}`,
        phone: faker.phone.phoneNumber(),
        addressBook: [{ destination: faker.address.streetAddress(), alias: 'default' }],
      });
      user.generateAuthToken();
      return user;
    });
}
