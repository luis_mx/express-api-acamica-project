const log = require('../server/config/logger');
const { Timer } = require('../utils');
const createMockUsers = require('./createMockUsers');
const createMockPayments = require('./createMockPayments');
const createMockProducts = require('./createMockProducts');

const timer = new Timer('seeding');

(async function SeedDB() {
  timer.start();
  try {
    await createMockUsers();
    await createMockPayments();
    await createMockProducts();
    timer.end();
  } catch (error) {
    log.fatal(error.stack);
  } finally {
    process.exit();
  }
})();
