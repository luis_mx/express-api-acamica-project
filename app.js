const express = require('express');
require('dotenv').config();
const helmet = require('helmet');
const swaggerUI = require('swagger-ui-express');
const swaggerJsDoc = require('swagger-jsdoc');

const { PORT: port, APIVERSION: vAPI } = require('./server/config/env');
const apiRouter = require('./routes');
const { swaggerOptions } = require('./server/swagger/options.swagger');
const { errorHandler } = require('./middleware');
const log = require('./server/config/logger');

const app = express();

app.use(helmet());
app.use(express.json());

app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerJsDoc(swaggerOptions)));

app.use(`/v${vAPI}`, apiRouter);

app.use('*', (req, res) => res.status(404).send({ status: '404', message: 'Dead end!!' }));

app.use(errorHandler);

module.exports = app.listen(port, () => log.info(`http://127.0.0.1:${port}/api-docs`));
