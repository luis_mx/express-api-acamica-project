const order = require('./order.controller');
const payment = require('./payment.controller');
const product = require('./product.controller');
const user = require('./user.controller');

module.exports = {
  order,
  payment,
  product,
  user,
};
