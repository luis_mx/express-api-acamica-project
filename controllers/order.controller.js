const { OrderStatus } = require('../models');
const { throwError } = require('../utils');
const { Order } = require('../repository');

const isOrderInStore = Order.isInStore();

const findAllOrders = async (req, res, next) => {
  try {
    const orders = await Order.findAll();
    res.send(orders);
  } catch (error) {
    next(error);
  }
};

const createOrder = async ({ body: { productId, quantity } }, res, next) => {
  const payload = { productId, quantity, user: res.locals.user };
  try {
    const order = await Order.create(payload);
    res.status(201).send(order);
  } catch (error) {
    next(error);
  }
};

const updateOrderStatus = async ({ params, body }, res, next) => {
  const { id } = params;
  const { statusCode } = body;
  try {
    OrderStatus[statusCode] ?? throwError('Unknown Status Code');
    await Order.update(id, 'status', OrderStatus[statusCode]);
    const message = `Order status has been updated to ${OrderStatus[statusCode]}`;
    res.send({ status: 'ok', message });
  } catch (error) {
    next(error);
  }
};

const updateOrderAddEditProduct = async ({ params, body }, res, next) => {
  const { id } = params;
  const { productId, quantity } = body;
  try {
    await Order.updateAddEditProduct(id, productId, quantity, res.locals.user);
    res.send({ status: 'ok', message: `Order updated` });
  } catch (error) {
    next(error);
  }
};

const updateOrderDestination = async ({ params: { id }, body: { destination } }, res, next) => {
  try {
    await Order.update(id, 'destination', destination, res.locals.user);
    res.send({ status: 'ok', message: `Updated order's destination` });
  } catch (error) {
    next(error);
  }
};

const deleteProductFromOrder = async ({ params: { id }, body: { productId } }, res, next) => {
  try {
    await Order.deleteProduct(id, productId, res.locals.user);
    res.send({ status: 'ok', message: `Product removed from order` });
  } catch (error) {
    next(error);
  }
};

module.exports = {
  isOrderInStore,
  findAllOrders,
  createOrder,
  updateOrderStatus,
  updateOrderAddEditProduct,
  updateOrderDestination,
  deleteProductFromOrder,
};
