const { Payment } = require('../repository');
const { sanitize, throwError } = require('../utils');

const isPaymentInStore = Payment.isInStore();

const findAllPayments = async (req, res, next) => {
  try {
    const payments = await Payment.findAll();
    return res.send(payments);
  } catch {
    next(error);
  }
};

const createPayment = async ({ body: { type, name } }, res, next) => {
  const data = sanitize.deleteNotStringKeys({ type, name });
  if (!data.type || !data.name) throwError('Missing requirements');
  try {
    const payment = await Payment.create(data);
    return res.status(201).send(payment);
  } catch {
    next(error);
  }
};

const updatePayment = async ({ params: { id }, body: { type, name } }, res, next) => {
  const data = sanitize.deleteNotStringKeys({ type, name });
  try {
    await Payment.update(id, data);
    res.status(204).send({ status: 'ok', message: `Payment ${id} has been updated` });
  } catch {
    next(error);
  }
};

const deletePayment = async ({ params: { id } }, res, next) => {
  try {
    const { deletedCount } = await Payment.delete(id);
    if (!Boolean(deletedCount)) throwError('Unable to delete payment from database');
    res.status(204).send({ status: 'ok', message: `Payment ${id} is no longer in store` });
  } catch {
    next(error);
  }
};

module.exports = {
  isPaymentInStore,
  findAllPayments,
  createPayment,
  updatePayment,
  deletePayment,
};
