const { redisUnlink } = require('../server/config/redis');
const { sanitize, throwError, saveArrToRedis, Timer } = require('../utils');
const { Product } = require('../repository');

const isProductInStore = Product.isInStore();
const allProductsTimer = new Timer('retrieving products from database');
const productTimer = new Timer('retrieving product from database');

const findAllProducts = async (req, res, next) => {
  try {
    allProductsTimer.start();
    const products = await Product.findAll();
    res.send(products);
    await saveArrToRedis(products, 'product');
    allProductsTimer.end();
  } catch (error) {
    next(error);
  }
};
const findProduct = async ({ params: { id } }, res, next) => {
  try {
    productTimer.start();
    const product = await Product.find(id);
    res.send(product);
    await saveArrToRedis([product], 'product');
    productTimer.end();
  } catch (error) {
    next(error);
  }
};

const createProduct = async ({ body: { name, price } }, res, next) => {
  try {
    const product = await Product.create({ name, price });
    res.status(201).send(product);
    return await saveArrToRedis([product], 'product');
  } catch (error) {
    next(error);
  }
};

const updateProduct = async ({ params: { id }, body: { name, price } }, res, next) => {
  try {
    const updates = sanitize.deleteNotStringNotNumberKeys({ name, price });
    const product = await Product.update(id, updates);
    res.status(204).send({ status: 'ok', message: `Product ${id} has been updated` });
    return await saveArrToRedis([product], 'product');
  } catch (error) {
    next(error);
  }
};

const deleteProduct = async ({ params: { id } }, res, next) => {
  try {
    const { deletedCount } = await Product.delete(id);
    if (!Boolean(deletedCount)) throwError('Unable to delete product from database');
    res.status(204).send({ status: 'ok', message: `Product ${id} is no longer in store` });
    return await redisUnlink(`product:${id}`);
  } catch (error) {
    next(error);
  }
};

module.exports = {
  isProductInStore,
  findAllProducts,
  findProduct,
  createProduct,
  updateProduct,
  deleteProduct,
};
