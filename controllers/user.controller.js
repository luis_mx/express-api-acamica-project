const { OrderStatus } = require('../models');
const { sanitize, throwError } = require('../utils');
const { User } = require('../repository');

const isUserInStore = User.isInStore();

const findAllUsers = async (req, res, next) => {
  try {
    const users = await User.findAll();
    return res.send(users);
  } catch (error) {
    next(error);
  }
};

const findUser = async ({ params: { id } }, res, next) => {
  const select = ['username', 'email', 'fullName', 'phone', 'addressBook', 'hasAccess', 'token'];
  try {
    const user = await User.find(id, select);
    return res.send(user);
  } catch (error) {
    next(error);
  }
};

const findUserAllOrders = async ({ params: { id } }, res, next) => {
  try {
    const { orders } = await User.find(id, ['orders']);
    const body = orders.length ? orders : { status: 'ok', message: 'User has no orders' };
    return res.send(body);
  } catch (error) {
    next(error);
  }
};

const findUserOrder = async ({ params: { id, orderId } }, res, next) => {
  try {
    const order = await User.findOrder(id, orderId);
    return res.send(order);
  } catch (error) {
    next(error);
  }
};

const logInUser = async ({ body: { email, password } }, res, next) => {
  try {
    const user = (await User.login(email, password)) ?? throwError('Error retrieving user');
    if (!user.hasAccess) throwError('User access has been revoked');
    const token = await user.generateAuthTokenAndSave();

    return res.send({ status: 'ok', token });
  } catch (error) {
    next(error);
  }
};

const createUser = async ({ body }, res, next) => {
  const { username, password, email, fullName, phone, address, isAdmin } = body;
  const userInfo = sanitize.deleteNotStringKeys({ username, password, email, fullName, phone });
  try {
    const user = await User.create(
      { ...userInfo, isAdmin: typeof isAdmin === 'boolean' ? isAdmin : false },
      address
    );

    return res.status(201).send(user);
  } catch (error) {
    next(error);
  }
};

const updateUserInfo = async ({ params: { id }, body }, res, next) => {
  const { username, password, email, fullName, phone } = body;
  const updates = sanitize.deleteNotStringKeys({ username, password, email, fullName, phone });
  try {
    await User.update(id, updates);
    res.send({ status: 'ok', message: `Updated user's info` });
  } catch (error) {
    next(error);
  }
};

const updateUserRole = async ({ params: { id }, body: { isAdmin } }, res, next) => {
  if (typeof isAdmin !== 'boolean') throwError('Value must be a boolean');
  try {
    await User.update(id, { isAdmin });
    const message = `${isAdmin ? 'Granted' : 'Revoked'} user:${id} admin privilege`;
    res.send({ status: 'ok', message });
  } catch (error) {
    next(error);
  }
};

const updateUserAccess = async ({ params: { id }, body: { hasAccess } }, res, next) => {
  if (typeof hasAccess !== 'boolean') throwError('Value must be a boolean');
  try {
    await User.update(id, { hasAccess });
    const message = `${hasAccess ? 'Granted' : 'Revoked'} user:${id} access`;
    res.send({ status: 'ok', message });
  } catch (error) {
    next(error);
  }
};

const updateUserAddOrEditAddress = async ({ params, body }, res, next) => {
  const { id } = params;
  const { address: destination, alias } = body;
  try {
    await User.updateAddressBook(id, { destination, alias });
    res.send({ status: 'ok', message: 'Address saved' });
  } catch (error) {
    next(error);
  }
};

const updateUserDeleteAddress = async ({ params: { id }, body: { alias } }, res, next) => {
  if (alias === 'default') throwError('Default address can not be deleted');
  try {
    await User.deleteAddress(id, alias);
    res.send({ status: 'ok', message: 'Address removed' });
  } catch (error) {
    next(error);
  }
};

const deleteUser = async ({ params: { id } }, res, next) => {
  try {
    const { deletedCount } = await User.delete(id);
    if (!Boolean(deletedCount)) throwError('Unable to delete user from database');
    res.send({ status: 'ok', message: `User ${id} is no longer in store` });
  } catch (error) {
    next(error);
  }
};

module.exports = {
  isUserInStore,
  findAllUsers,
  findUser,
  findUserAllOrders,
  findUserOrder,
  logInUser,
  createUser,
  updateUserInfo,
  updateUserRole,
  updateUserAccess,
  updateUserAddOrEditAddress,
  updateUserDeleteAddress,
  deleteUser,
};
