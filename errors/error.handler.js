const log = require('../server/config/logger');
const err = { status: 'error' };

module.exports = (error, res) => {
  // https://datatracker.ietf.org/doc/html/rfc7231#page-61
  const { message } = error;

  // key duplication MongoDB
  if (error.code === 11000) {
    const [key, value] = JSON.stringify(error.keyValue)
      .replace(/[\"{}]/g, '')
      .split(':');
    return res.status(200).send({ ...err, message: `${key} : ${value}, is already in store` });
  }

  if (error.kind === 'ObjectId') {
    const model = error.message.match(/model\s"(?<model>\w+)"/).groups?.model;
    const message = `${model && model + ' '}ID not found`;
    return res.status(404).send({ status: '404', message });
  }

  if (error.kind === 'required') {
    if (error.path)
      return res.status(400).send({ ...err, message: `${error.path} value is required` });
  }

  if (error.name === 'NotFoundError') return res.status(404).send({ status: '404', message });

  // Unhandled errors ---------------------------------------------------------
  // log.fatal(error);
  log.fatal(error.stack);
  return res.status(500).send({ ...err, message });
};
