const NotFoundError = require('./NotFoundError');
const ErrorHandler = require('./error.handler');

module.exports = {
  NotFoundError,
  ErrorHandler,
};
