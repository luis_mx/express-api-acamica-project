const jwt = require('jsonwebtoken');
const { JWT_KEY } = require('../server/config/env');
const { User } = require('../models');
const { throwError } = require('../utils');

exports.authorizerIsUser = async (req, res, next) => {
  const token = req.header('Authorization')?.replace('Bearer ', '');
  try {
    const { _id } = jwt.verify(token, JWT_KEY);
    const user =
      (await User.findOne({ _id, token }).lean().select(['addressBook', 'hasAccess'])) ??
      throwError('No user found');
    if (!user.hasAccess) throwError('User access has been revoked');
    res.locals.user = user;
    next();
  } catch (error) {
    const { message } = error;
    if (message === 'No user found') return res.status(401).send({ status: '401', message });
    next(error);
  }
};

exports.authorizerIsAdmin = async (req, res, next) => {
  const token = req.header('Authorization')?.replace('Bearer ', '');
  try {
    const { _id } = jwt.verify(token, JWT_KEY);
    const user =
      (await User.findOne({ _id, token }).lean().select(['isAdmin', 'hasAccess', 'token'])) ??
      throwError('No user found');
    return user.isAdmin && user.hasAccess
      ? next()
      : throwError('User does not have the credentials to access this resources');
  } catch (error) {
    const { message } = error;
    if (message === 'No user found') return res.status(401).send({ status: '401', message });
    next(error);
  }
};

exports.authorizerIsCurrentUserOrIsAdmin = async (req, res, next) => {
  const token = req.header('Authorization')?.replace('Bearer ', '');
  try {
    const { _id } = jwt.verify(token, JWT_KEY);
    const user =
      (await User.findOne({ _id, token }).lean().select(['isAdmin', 'hasAccess', 'token'])) ??
      throwError('No user found');
    if (_id !== req.params.id && !user.isAdmin) throwError("Can't access this resource");
    if (!user.hasAccess) throwError('User access has been revoked');
    next();
  } catch (error) {
    const { message } = error;
    if (message === 'No user found') return res.status(401).send({ status: '401', message });
    if (message === 'jwt must be provided') error.message = 'invalid token';
  }
};
