const { ErrorHandler } = require('../errors');

module.exports = (error, req, res, next) => ErrorHandler(error, res);
