const log = require('../server/config/logger');

const { redisFlushAll } = require('../server/config/redis');

module.exports = async (req, res, next) => {
  try {
    await redisFlushAll();
    log.info(`flushed product's cache`);
    res.send({ status: 'ok', message: 'cache deleted' });
  } catch (error) {
    next(error);
  }
};
