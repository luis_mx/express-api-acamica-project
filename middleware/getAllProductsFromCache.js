const { redisMGet } = require('../server/config/redis');
const { Product } = require('../models');
const { retrieveProductKeysFromCache, Timer } = require('../utils');

const timer = new Timer('retrieving products from cache');

module.exports = async (req, res, next) => {
  try {
    timer.start();

    const dbProductLength = await Product.estimatedDocumentCount();
    const productKeys = await retrieveProductKeysFromCache();

    if (productKeys.length !== dbProductLength) return next();

    const productsJSON = await Promise.all(await redisMGet(productKeys));
    const products = productsJSON.map((product) => JSON.parse(product));

    timer.end();

    return res.send(products);
  } catch (error) {
    next(error);
  }
};
