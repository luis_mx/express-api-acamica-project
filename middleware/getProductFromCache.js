const { redisMGet } = require('../server/config/redis');
const log = require('../server/config/logger');

module.exports = async ({ params: { id: productId } }, res, next) => {
  try {
    const [product] = await redisMGet(`product:${productId}`);
    return product
      ? log.info('retrieving product from cache') || res.send(JSON.parse(product))
      : next();
  } catch (error) {
    next(error);
  }
};
