const { authorizerIsUser, authorizerIsAdmin, authorizerIsCurrentUserOrIsAdmin } = require('./authorize');
const errorHandler = require('./error.middleware');
const flushAllProductsFromCache = require('./flushAllProductsFromCache');
const getAllProductsFromCache = require('./getAllProductsFromCache');
const getProductFromCache = require('./getProductFromCache');
const isInStore = require('./isInStore');
const isOrderOpen = require('./isOrderOpen');
const productExist = require('./productExist');

module.exports = {
  authorizerIsUser,
  authorizerIsAdmin,
  authorizerIsCurrentUserOrIsAdmin,
  errorHandler,
  flushAllProductsFromCache,
  getAllProductsFromCache,
  getProductFromCache,
  isInStore,
  isOrderOpen,
  productExist,
};
