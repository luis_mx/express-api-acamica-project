const mongoose = require('../server/config/db');
const { NotFoundError } = require('../errors');

module.exports = (Model) =>
  async function isInStore({ params: { id: _id } }, res, next) {
    try {
      if (mongoose.isValidObjectId(_id) && (await Model.exists({ _id }))) return next();
      throw new NotFoundError(`No ${Model.modelName.toLowerCase()} found`);
    } catch (error) {
      next(error);
    }
  };
