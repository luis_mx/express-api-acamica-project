const { Order, OrderStatus } = require('../models');

module.exports = async ({ params: { id: _id } }, res, next) => {
  try {
    const order = await Order.findOne({ _id }).lean();
    return order.status === OrderStatus[0]
      ? next()
      : res.send({ status: 'closed', message: `Order status is not ${OrderStatus[0]}` });
  } catch (error) {
    next(error);
  }
};
