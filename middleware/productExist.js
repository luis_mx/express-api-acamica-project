const { Product } = require('../models');

module.exports = async ({ body: { productId: _id } }, res, next) => {
  try {
    return (await Product.exists({ _id }))
      ? next()
      : res.status(404).json({ status: '404', message: `No product found` });
  } catch (error) {
    next(error);
  }
};
