module.exports = {
  User: require('./user.model').User,
  Order: require('./order.model').Order,
  Payment: require('./payment.model').Payment,
  Product: require('./product.model').Product,
  OrderStatus: require('./order.status.enum'),
}