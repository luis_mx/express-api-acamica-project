const { model, Schema } = require('../server/config/db');
const { ProductSchema } = require('./product.model');
const { PaymentSchema } = require('./payment.model');
const OrderStatus = require('./order.status.enum');

exports.OrderSchema = new Schema({
  date: { type: Date, default: Date.now(), inmutable: true },
  status: { type: String, required: true, default: OrderStatus[0], enum: OrderStatus },
  destination: String,
  paymentMethod: PaymentSchema,
  products: [
    {
      product: ProductSchema,
      quantity: {
        type: Number,
        default: 1,
      },
    },
  ],
});

const OrderSchema = exports.OrderSchema.clone();

// require('../server/config/logger').info(OrderSchema.indexes());
// sum total in a method or static method here
// total should be a virtual

exports.Order = model('Order', OrderSchema);
