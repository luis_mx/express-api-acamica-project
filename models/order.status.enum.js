const OrderStatus = ['PENDING', 'CONFIRMED', 'PREPARATION', 'SEND', 'COMPLETED'];

module.exports = OrderStatus;
