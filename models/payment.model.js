const { model, Schema } = require('../server/config/db');

exports.PaymentSchema = new Schema({
  type: { type: String, required: true, uppercase: true },
  name: { type: String, required: true },
});

const PaymentSchema = exports.PaymentSchema.clone();
PaymentSchema.path('type').index({ unique: true });

// const log = require('../server/config/logger');
// log.info(exports.PaymentSchema.indexes());
// log.info(PaymentSchema.indexes());

exports.Payment = model('Payment', PaymentSchema);

