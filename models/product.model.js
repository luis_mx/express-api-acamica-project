const { model, Schema } = require('../server/config/db');

exports.ProductSchema = new Schema({
  name: { type: String, required: true },
  price: { type: Number, default: 0 },
});

const ProductSchema = exports.ProductSchema.clone();
ProductSchema.path('name').index({ unique: true });

// const log = require('../server/config/logger');
// log.info(exports.ProductSchema.indexes());
// log.info(ProductSchema.indexes());

exports.Product = model('Product', ProductSchema);
