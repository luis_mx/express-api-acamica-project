const jwt = require('jsonwebtoken');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const { JWT_KEY } = require('../server/config/env');
const { model, Schema } = require('../server/config/db');
const { OrderSchema } = require('./order.model');
const { throwError } = require('../utils');

const UserSchema = new Schema({
  username: { type: String, required: true, trim: true },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    validate: (email) => validator.isEmail(email) || throwError(`invalid: ${email}`),
  },
  password: { type: String, required: true, minLength: 5 },
  addressBook: {
    type: [
      {
        destination: { type: String, required: true },
        alias: { type: String, required: true },
      },
    ],
    validate: (addressBook) => Array.isArray(addressBook) && addressBook.length > 0,
  },
  isAdmin: { type: Boolean, default: false },
  hasAccess: { type: Boolean, default: true },
  orders: [OrderSchema],
  fullName: String,
  phone: String,
  token: String,
});

// hash password before saving
UserSchema.pre('save', async function (next) {
  const user = this;
  if (user.isModified('password')) user.password = await bcrypt.hash(user.password, 10);
  next();
});

// generate auth token for current user and save
UserSchema.methods.generateAuthTokenAndSave = async function () {
  const user = this;
  try {
    const token = jwt.sign({ _id: user._id }, JWT_KEY);
    user.token = token;
    await user.save();
    return token;
  } catch (error) {
    throw error;
  }
};

// generate auth token for current user
UserSchema.methods.generateAuthToken = async function () {
  const user = this;
  try {
    const token = jwt.sign({ _id: user._id }, JWT_KEY);
    user.token = token;
    return { status: 'ok' };
  } catch (error) {
    throw error;
  }
};

// search user by email & password combination
UserSchema.statics.findByCredentials = async function (email, password) {
  try {
    const user = (await User.findOne({ email })) ?? throwError(`No user found ${email}`);
    !(await bcrypt.compare(password, user.password)) && throwError(`Invalid login credentials.`);
    return user;
  } catch (error) {
    throw error;
  }
};

const User = model('User', UserSchema);

// require('../server/config/logger').info(UserSchema.indexes());

exports.User = User;
