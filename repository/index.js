const Product = require('./product.repository');
const User = require('./user.repository');
const Payment = require('./payment.repository')
const Order = require('./order.repository')

module.exports = {
  Product,
  Payment,
  User,
  Order,
}