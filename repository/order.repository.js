const mongoose = require('../server/config/db');
const { throwError } = require('../utils');
const { isInStore } = require('../middleware');
const { Order } = require('../models');
const Product = require('./product.repository');
const User = require('./user.repository');

module.exports = {
  isInStore: () => isInStore(Order),
  findAll: () => Order.find().lean(),
  create: async ({ productId, quantity, user }) => {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const product = await Product.find(productId);
      const order = new Order({
        destination: user.addressBook[0].destination,
        products: [{ product, quantity }],
      });

      await order.save();
      await User.update(user._id, { $push: { orders: order } });
      await session.commitTransaction();

      return order;
    } catch (error) {
      session.abortTransaction();
      throw error;
    } finally {
      session.endSession();
    }
  },
  update: async (_id, key, data, user) => {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      await Order.updateOne({ _id }, { [key]: data });
      if (key === 'status') await User.updateOrderStatus(_id, data);
      else await User.updateOrder(user._id, _id, { [`orders.$.${key}`]: data });

      await session.commitTransaction();
      return 'ok';
    } catch (error) {
      session.abortTransaction();
      throw error;
    } finally {
      session.endSession();
    }
  },
  updateAddEditProduct: async (_id, productId, quantity, user) => {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const order = await Order.findOne({ _id });
      let product = await order.products.find(({ product }) => product._id.equals(productId));
      if (product) {
        product.quantity += quantity;
        if (product.quantity < 1) product.remove();
      } else {
        product = await Product.find(productId);
        order.products.push({ product, quantity });
      }

      await order.save();
      await User.updateOrder(user._id, _id, { 'orders.$.products': [...order.products] });

      await session.commitTransaction();
      return 'ok';
    } catch (error) {
      session.abortTransaction();
      throw error;
    } finally {
      session.endSession();
    }
  },
  deleteProduct: async (_id, productId, user) => {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const order = await Order.findOne({ _id });
      const product =
        (await order.products.find(({ product }) => product._id.equals(productId))) ??
        throwError(`Product not found in order.`);

      product.remove();
      await order.save();
      await User.updateOrder(user._id, _id, { 'orders.$.products': [...order.products] });

      await session.commitTransaction();
      return 'ok';
    } catch (error) {
      session.abortTransaction();
      throw error;
    } finally {
      session.endSession();
    }
  },
};
