const { isInStore } = require('../middleware');
const { Payment } = require('../models');

module.exports = {
  isInStore: () => isInStore(Payment),
  findAll: () => Payment.find().lean(),
  create: ({ type, name }) => new Payment({type, name}).save(),
  update: (_id, updates) => Payment.updateOne({ _id }, updates),
  delete: (_id) => Payment.deleteOne({ _id }),
};
