const { isInStore } = require('../middleware');
const { Product } = require('../models');

module.exports = {
  isInStore: () => isInStore(Product),
  // https://stackoverflow.com/questions/5830513/how-do-i-limit-the-number-of-returned-items
  // const products = await Product.find({}, null, { limit: 50, skip:50, }).lean();
  findAll: () => Product.find().lean(),
  find: (_id) => Product.findById(_id).lean(),
  create: ({ name, price }) => new Product({ name, price }).save(),
  update: (_id, updates) => Product.findByIdAndUpdate(_id, updates, { new: true }).lean(),
  delete: (_id) => Product.deleteOne({ _id }),
};
