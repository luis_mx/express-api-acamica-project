const { isInStore } = require('../middleware');
const { throwError } = require('../utils');
const { User } = require('../models');

module.exports = {
  isInStore: () => isInStore(User),
  findAll: () => User.find().select(['-orders', '-password', '-isAdmin', '-hasAccess']).lean(),
  find: (_id, select = []) => User.findById(_id).select(select).lean(),
  findOrder: async (_id, orderId) => {
    const user = await User.findById(_id).select(['orders']);
    const order = user.orders.id(orderId) ?? throwError('No order found');
    return order;
  },
  login: (email, password) => User.findByCredentials(email, password),
  create: async (userInfo, address) => {
    const user = new User(userInfo);
    if (typeof address === 'string' && address !== '')
      user.addressBook.push({ destination: address, alias: 'default' });
    await user.generateAuthTokenAndSave();
    return {
      status: 'ok',
      token: user.token,
      user: {
        _id: user.id,
        username: user.username,
        email: user.email,
        fullName: user.fullName,
        phone: user.phone,
        addressBook: user.addressBook,
      },
    };
  },
  update: (_id, updates) => User.updateOne({ _id }, updates),
  updateOrder: (_id, orderId, updates) => User.updateOne({ _id, 'orders._id': orderId }, updates),
  updateOrderStatus: (orderId, status) => User.updateOne({ 'orders._id': orderId }, {'orders.$.status': status}),
  updateAddressBook: async (_id, { destination, alias }) => {
    const user = await User.findOne({ _id }, ['addressBook']);
    const address = await user.addressBook.find((address) => address.alias === alias);
    if (address) {
      if (address.destination === destination) return;
      address.destination = destination;
    } else {
      user.addressBook.push({ destination, alias });
    }
    return await user.save();
  },
  delete: (_id) => User.deleteOne({ _id }),
  deleteAddress: async (_id, alias) => {
    const user = await User.findById(_id).select(['addressBook']);
    const address = await user.addressBook.find((address) => address.alias === alias);
    if (!address) throwError('Address not found');
    address.remove();
    return await user.save();
  },
  // updateToken: (user) => user.generateAuthTokenAndSave(),
  // create: ({ name, price }) => new Product({ name, price }).save(),
  // update: (_id, updates) => Product.findByIdAndUpdate(_id, updates, { new: true }).lean(),
};
