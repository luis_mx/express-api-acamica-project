const router = require('express').Router();
const { logInUser } = require('../../controllers').user;

/**
 *  @swagger
 *  /login:
 *  post:
 *    tags: ['Log In & Sign Up']
 *    summary: Logs in user
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              email:
 *                type: string
 *              password:
 *                type: string
 *          examples:
 *            Administrator:
 *              value:
 *                email: admin@email.com
 *                password: admin
 *            Regular User:
 *              value:
 *                email: basico@email.com
 *                password: lalala
 *    responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/User'
 *      401:
 *        description: Authentication failed
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: boolean
 *                  example: false
 *                message:
 *                  type: string
 *                  example: Incorrect login data.
 */
router.post('/', logInUser);

module.exports = router;
