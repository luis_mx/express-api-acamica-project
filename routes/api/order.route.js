const router = require('express').Router();

const {
  authorizerIsUser,
  authorizerIsAdmin,
  productExist,
  isOrderOpen,
} = require('../../middleware');

const {
  isOrderInStore,
  findAllOrders,
  createOrder,
  updateOrderStatus,
  updateOrderAddEditProduct,
  updateOrderDestination,
  deleteProductFromOrder,
} = require('../../controllers').order;

/**
 *  @swagger
 *  /orders:
 *  get:
 *    tags: [Orders]
 *    summary: Shows all orders
 *    security:
 *      - bearerAuth: []
 *    description: Admins ONLY
 *    consumes:
 *      - application/json
 *    responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Order'
 *      401:
 *        $ref: '#/components/responses/401'
 */
router.get('/', authorizerIsAdmin, findAllOrders);

/**
 *  @swagger
 *  /orders:
 *  post:
 *    tags: [Orders]
 *    summary: Creates a new order
 *    security:
 *      - bearerAuth: []
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              productId:
 *                type: string
 *                example: 6132e81ad91dae30eecb2ce8
 *              quantity:
 *                type: integer
 *                example: 3
 *    responses:
 *      201:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Order'
 *            example:
 *              products:
 *                - productId: 66
 *                  quantity: 6
 *              total: 666
 *              status: PENDING
 *              date: 2021-06-01T11:46:26.129Z
 *              userId: 23
 *              id: 1622547986129
 *      401:
 *        $ref: '#/components/responses/401'
 */
router.post('/', authorizerIsUser, createOrder);

/**
 *  @swagger
 *  /orders/{id}/status:
 *  put:
 *    tags: [Orders]
 *    summary: Updates order status
 *    security:
 *      - bearerAuth: []
 *    description: Admins ONLY
 *    parameters:
 *      - $ref: '#/components/parameters/orderParam'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              statusCode:
 *                type: string
 *                example: 3
 *    responses:
 *      200:
 *        $ref: '#/components/responses/ok'
 *      422:
 *        description: Unknown status code
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *               status:
 *                type: boolean
 *                example: false
 *               message:
 *                type: string
 *                example: Unknown status code.
 *      404:
 *        $ref: '#/components/responses/404'
 */
router.put('/:id/status', authorizerIsAdmin, isOrderInStore, updateOrderStatus);

/**
 *  @swagger
 *  /orders/{id}/destination:
 *  put:
 *    tags: [Orders]
 *    summary: Modifies order's address destination
 *    security:
 *      - bearerAuth: []
 *    description: 'Login using Basic User Credentials from the dropdown menu'
 *    parameters:
 *      - $ref: '#/components/parameters/orderParam'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              destination:
 *                type: string
 *                example: Nowhere Street No. 123
 *    responses:
 *      200:
 *        $ref: '#/components/responses/ok'
 *      401:
 *        $ref: '#/components/responses/401'
 */
router.put(
  '/:id/destination',
  authorizerIsUser,
  isOrderInStore,
  isOrderOpen,
  updateOrderDestination
);

/**
 *  @swagger
 *  /orders/{id}/product:
 *  put:
 *    tags: [Orders]
 *    summary: Adds / modifies order's product
 *    security:
 *      - bearerAuth: []
 *    description: If product ID is repeated, quantity gets modified
 *    parameters:
 *      - $ref: '#/components/parameters/orderParam'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              productId:
 *                type: string
 *                example: 6132e81ad91dae30eecb2ce9
 *              quantity:
 *                type: integer
 *                example: 2
 *    responses:
 *      200:
 *        $ref: '#/components/responses/ok'
 *      401:
 *        $ref: '#/components/responses/401'
 *      404:
 *        $ref: '#/components/responses/404'
 */
router.put('/:id/product', authorizerIsUser, isOrderInStore, productExist, updateOrderAddEditProduct);

/**
 *  @swagger
 *  /orders/{id}/product:
 *  delete:
 *    tags: [Orders]
 *    summary: Deletes product from order
 *    security:
 *      - bearerAuth: []
 *    description: 'Login using Basic User Credentials from the dropdown menu'
 *    parameters:
 *      - $ref: '#/components/parameters/orderParam'
  *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              productId:
 *                type: string
 *                example: 6132e81ad91dae30eecb2ce9
 *    responses:
 *      200:
 *        $ref: '#/components/responses/ok'
 *      401:
 *        $ref: '#/components/responses/401'
 *      404:
 *        $ref: '#/components/responses/404'
 */
router.delete(
  '/:id/product/',
  authorizerIsUser,
  isOrderInStore,
  productExist,
  isOrderOpen,
  deleteProductFromOrder
);

module.exports = router;
