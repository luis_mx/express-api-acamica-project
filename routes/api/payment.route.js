const router = require('express').Router();
const { authorizerIsAdmin } = require('../../middleware');
const { isPaymentInStore, findAllPayments, createPayment, updatePayment, deletePayment } =
  require('../../controllers').payment;

/**
 *  @swagger
 *  /payments:
 *  get:
 *    tags: [Payments]
 *    summary: Shows all payments
 *    security:
 *      - bearerAuth: []
 *    consumes:
 *      - application/json
 *    responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Payment'
 *      401:
 *        $ref: '#/components/responses/401'
 */
router.get('/', authorizerIsAdmin, findAllPayments);

/**
 *  @swagger
 *  /payments:
 *  post:
 *    tags: [Payments]
 *    summary: Creates new payment
 *    security:
 *      - bearerAuth: []
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              type:
 *                type: string
 *              name:
 *                type: string
 *            example:
 *              type: MP
 *              name: MercadoPago
 *    responses:
 *      201:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Payment'
 *            example:
 *              type: MP
 *              name: MercadoPago
 *              id: 1621823390569
 *      401:
 *        $ref: '#/components/responses/401'
 */
router.post('/', authorizerIsAdmin, createPayment);

/**
 *  @swagger
 *  /payments/{id}:
 *  put:
 *    tags: [Payments]
 *    summary: Updates payment's name or type
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - $ref: '#/components/parameters/paymentParam'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              type:
 *                type: string
 *              name:
 *                type: string
 *          examples:
 *            Name:
 *              value:
 *                name: Plata
 *            Type & Name:
 *              value:
 *                price: PLATA
 *                name: Efe
 *    responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Payment'
 *            example:
 *              type: CASH
 *              name: Plata
 *              id: 2
 *      401:
 *        $ref: '#/components/responses/401'
 *      404:
 *        $ref: '#/components/responses/404'
 */
router.put('/:id', authorizerIsAdmin, isPaymentInStore, updatePayment);

/**
 *  @swagger
 *  /payments/{id}:
 *  delete:
 *    tags: [Payments]
 *    summary: Deletes payment
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - $ref: '#/components/parameters/paymentParam'
 *    responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: boolean
 *                  example: true
 *                message:
 *                  type: string
 *                  example: Payment (ID:2) - «CASH», is no longer stored.
 *      401:
 *        $ref: '#/components/responses/401'
 *      404:
 *        $ref: '#/components/responses/404'
 */
router.delete('/:id', authorizerIsAdmin, isPaymentInStore, deletePayment);

module.exports = router;
