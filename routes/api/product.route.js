const router = require('express').Router();

const {
  authorizerIsAdmin,
  flushAllProductsFromCache,
  getAllProductsFromCache,
  getProductFromCache,
} = require('../../middleware');

const {
  isProductInStore,
  findAllProducts,
  findProduct,
  createProduct,
  updateProduct,
  deleteProduct,
} = require('../../controllers').product;

router.get('/flush-cache', flushAllProductsFromCache);

/**
 *  @swagger
 *  /products:
 *  get:
 *    tags: [Products]
 *    summary: Shows all products
 *    security:
 *      - bearerAuth: []
 *    consumes:
 *      - application/json
 *    responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Product'
 *      401:
 *        $ref: '#/components/responses/401'
 */
router.get('/', authorizerIsAdmin, getAllProductsFromCache, findAllProducts);

/**
 *  @swagger
 *  /products/{id}:
 *  get:
 *    tags: [Products]
 *    summary: Gets product
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - $ref: '#/components/parameters/productParam'
 *    responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Product'
 *      401:
 *        $ref: '#/components/responses/401'
 *      404:
 *        $ref: '#/components/responses/404'
 */
router.get('/:id', authorizerIsAdmin, isProductInStore, getProductFromCache, findProduct);

/**
 *  @swagger
 *  /products:
 *  post:
 *    tags: [Products]
 *    summary: Creates new product
 *    security:
 *      - bearerAuth: []
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              name:
 *                type: string
 *              price:
 *                type: integer
 *            example:
 *              name: Choripan
 *              price: 31000
 *    responses:
 *      201:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Product'
 *            example:
 *              name: Choripan
 *              price: 310
 *              id: 1621823390569
 *      401:
 *        $ref: '#/components/responses/401'
 */
router.post('/', authorizerIsAdmin, createProduct);

/**
 *  @swagger
 *  /products/{id}:
 *  put:
 *    tags: [Products]
 *    summary: Updates product's name or price
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - $ref: '#/components/parameters/productParam'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              name:
 *                type: string
 *              price:
 *                type: integer
 *          examples:
 *            Price:
 *              value:
 *                price: 12000
 *            Price & Name:
 *              value:
 *                name: Bágel de salmón Especial
 *                price: 9900
 *    responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Product'
 *            example:
 *              name: Bágel de salmón
 *              price: 999
 *              id: 99
 *      401:
 *        $ref: '#/components/responses/401'
 *      404:
 *        $ref: '#/components/responses/404'
 */
router.put('/:id', authorizerIsAdmin, isProductInStore, updateProduct);

/**
 *  @swagger
 *  /products/{id}:
 *  delete:
 *    tags: [Products]
 *    summary: Deletes product
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - $ref: '#/components/parameters/productParam'
 *    responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: boolean
 *                  example: true
 *                message:
 *                  type: string
 *                  example: Product (ID:99) - «Bágel de salmón», is no longer stored.
 *      401:
 *        $ref: '#/components/responses/401'
 *      404:
 *        $ref: '#/components/responses/404'
 */
router.delete('/:id', authorizerIsAdmin, isProductInStore, deleteProduct);

module.exports = router;
