const router = require('express').Router();
const { createUser } = require('../../controllers').user;

/**
 *  @swagger
 *  /signup:
 *  post:
 *    tags: ['Log In & Sign Up']
 *    summary: Creates a new user
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            required:
 *              - username
 *              - password
 *              - fullName
 *              - email
 *              - phone
 *            properties:
 *              username:
 *                type: string
 *              password:
 *                type: string
 *                format: password
 *              fullName:
 *                type: string
 *              email:
 *                type: string
 *              phone:
 *                type: string
 *              address:
 *                type: string
 *          example:
 *            username: albertoXYZ
 *            password: lalala
 *            fullName: Alberto Xavier
 *            email: alberto.xx@email.com
 *            phone: '+525523433654'
 *            address: Av de los Alamos 1, int 19
 *    responses:
 *      201:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                $ref: '#/components/schemas/User'
 *            example:
 *              username: albertoXYZ
 *              password: lalala
 *              fullName: Alberto Xavier
 *              email: alberto.xx@email.com
 *              phone: '+525523433654'
 *              address: Av de los Alamos 1, int 19
 *              id: 1622923011957
 *              isAdmin: false
 *      200:
 *        description: Duplicated email
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                status:
 *                  type: boolean
 *                  example: false
 *                message:
 *                  type: string
 *                  example: Email already registered
 */

router.post('/', createUser);

module.exports = router;
