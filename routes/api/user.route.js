const router = require('express').Router();

const {
  authorizerIsUser,
  authorizerIsAdmin,
  authorizerIsCurrentUserOrIsAdmin,
} = require('../../middleware');

const {
  isUserInStore,
  findAllUsers,
  findUser,
  findUserAllOrders,
  findUserOrder,
  updateUserInfo,
  updateUserRole,
  updateUserAddOrEditAddress,
  updateUserDeleteAddress,
  updateUserAccess,
  deleteUser,
} = require('../../controllers').user;

/**
 *  @swagger
 *  /users:
 *  get:
 *    tags: [Users]
 *    summary: Shows all users
 *    security:
 *      - bearerAuth: []
 *    consumes:
 *      - application/json
 *    responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/User'
 *      401:
 *        $ref: '#/components/responses/401'
 */
router.get('/', authorizerIsAdmin, findAllUsers);

/**
 *  @swagger
 *  /users/{id}:
 *  get:
 *    tags: [Users]
 *    summary: Gets user
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - $ref: '#/components/parameters/userParam'
 *    responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/User'
 *      401:
 *        $ref: '#/components/responses/401'
 *      404:
 *        $ref: '#/components/responses/404'
 */
router.get('/:id', authorizerIsCurrentUserOrIsAdmin, isUserInStore, findUser);

/**
 *  @swagger
 *  /users/{id}/orders:
 *  get:
 *    tags: [Users]
 *    summary: Gets user's orders history
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - $ref: '#/components/parameters/userParam'
 *    responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Order'
 *      401:
 *        $ref: '#/components/responses/401'
 *      404:
 *        $ref: '#/components/responses/404'
 */
router.get('/:id/orders', authorizerIsCurrentUserOrIsAdmin, isUserInStore, findUserAllOrders);

/**
 *  @swagger
 *  /users/{id}/orders/{orderId}:
 *  get:
 *    tags: [Users]
 *    summary: Gets user's order by id
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - $ref: '#/components/parameters/userParam'
 *      - $ref: '#/components/parameters/userOrderParam'
 *    responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Order'
 *      401:
 *        $ref: '#/components/responses/401'
 *      404:
 *        $ref: '#/components/responses/404'
 */
router.get('/:id/orders/:orderId', authorizerIsCurrentUserOrIsAdmin, isUserInStore, findUserOrder);

/**
 *  @swagger
 *  /users/{id}:
 *  put:
 *    tags: [Users]
 *    summary: Updates user's info
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - $ref: '#/components/parameters/userParam'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              username:
 *                type: string
 *                example: basurto_023
 *              fullName:
 *                type: string
 *                email: Emilio Uzcanga
 *          example:
 *    responses:
 *      200:
 *        $ref: '#/components/responses/ok'
 *      401:
 *        $ref: '#/components/responses/401'
 *      404:
 *        $ref: '#/components/responses/404'
 */
router.put('/:id', authorizerIsUser, isUserInStore, updateUserInfo);

/**
 *  @swagger
 *  /users/{id}/role:
 *  put:
 *    tags: [Users]
 *    summary: Change user's role
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - $ref: '#/components/parameters/userParam'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              isAdmin:
 *                type: boolean
 *                example: true
 *    responses:
 *      200:
 *        $ref: '#/components/responses/ok'
 *      401:
 *        $ref: '#/components/responses/401'
 *      404:
 *        $ref: '#/components/responses/404'
 */
router.put('/:id/role', authorizerIsAdmin, isUserInStore, updateUserRole);

/**
 *  @swagger
 *  /users/{id}/access:
 *  put:
 *    tags: [Users]
 *    summary: Disable/Enable user's access
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - $ref: '#/components/parameters/userParam'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              hasAccess:
 *                type: boolean
 *                example: false
 *    responses:
 *      200:
 *        $ref: '#/components/responses/ok'
 *      401:
 *        $ref: '#/components/responses/401'
 *      404:
 *        $ref: '#/components/responses/404'
 */
router.put('/:id/access', authorizerIsAdmin, isUserInStore, updateUserAccess);

/**
 *  @swagger
 *  /users/{id}/address:
 *  put:
 *    tags: [Users]
 *    summary: Add new address to user's address book
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - $ref: '#/components/parameters/userParam'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              address:
 *                type: string
 *              alias:
 *                type: string
 *          example:
 *            address: Av Siempre Viva 123, int 22
 *            alias: depa
 *    responses:
 *      200:
 *        $ref: '#/components/responses/ok'
 *      401:
 *        $ref: '#/components/responses/401'
 *      404:
 *        $ref: '#/components/responses/404'
 */
router.put(
  '/:id/address',
  authorizerIsCurrentUserOrIsAdmin,
  isUserInStore,
  updateUserAddOrEditAddress
);

/**
 *  @swagger
 *  /users/{id}/address:
 *  delete:
 *    tags: [Users]
 *    summary: Remove address from user's address book
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - $ref: '#/components/parameters/userParam'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              alias:
 *                type: string
 *                example: depa
 *    responses:
 *      200:
 *        $ref: '#/components/responses/ok'
 *      401:
 *        $ref: '#/components/responses/401'
 *      404:
 *        $ref: '#/components/responses/404'
 */
router.delete(
  '/:id/address',
  authorizerIsCurrentUserOrIsAdmin,
  isUserInStore,
  updateUserDeleteAddress
);

/**
 *  @swagger
 *  /users/{id}:
 *  delete:
 *    tags: [Users]
 *    summary: Deletes user
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - name: id
 *        in: path
 *        description: ''
 *        required: true
 *        schema:
 *          type: string
 *          format: byte
 *    responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              type: string
 *            example:
 *              'User ID:${user.id} (${user.username}) is no longer in store.'
 *      401:
 *        $ref: '#/components/responses/401'
 *
 *      404:
 *        $ref: '#/components/responses/404'
 */
router.delete('/:id', authorizerIsAdmin, isUserInStore, deleteUser);

module.exports = router;
