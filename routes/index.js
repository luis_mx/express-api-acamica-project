const router = require('express').Router();
const signupRoutes = require('./api/signup.route');
const loginRoutes = require('./api/login.route');
const userRoutes = require('./api/user.route');
const productRoutes = require('./api/product.route');
const orderRoutes = require('./api/order.route');
const paymentRoutes = require('./api/payment.route');

router.use('/signup', signupRoutes);
router.use('/login', loginRoutes);
router.use('/users', userRoutes);
router.use('/products', productRoutes);
router.use('/orders', orderRoutes);
router.use('/payments', paymentRoutes);

module.exports = router;