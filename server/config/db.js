const mongoose = require('mongoose');
const { throwError } = require('../../utils');
const { DBUSER, DBPASS, DBHOSTNAME, DBNAME } = require('./env');
const log = require('../config/logger');

const uri =
  DBHOSTNAME === 'localhost'
    ? `mongodb://${DBHOSTNAME}:27017/${DBNAME}?retryWrites=true&w=majority`
    : `mongodb+srv://${DBUSER}:${DBPASS}@${DBHOSTNAME}/${DBNAME}?retryWrites=true&w=majority`;

const connectionOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true, // needed for User schema unique email
};

mongoose
  .connect(uri, connectionOptions)
  .then(() => log.info('Connection to database successful'))
  .catch((error) => log.fatal(error) || throwError(error.message));

module.exports = mongoose;
