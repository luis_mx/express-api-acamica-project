const ENV = {
  PORT: process.env.PORT || 3456,
  JWT_KEY: process.env.JWT_KEY || 'ahfp@aisufaw&iufawpr#34*2847293%87a0-9sd8f7sd90f8y)45f^dsfg45*gsdg(654)67hfdghd',
  DBUSER: process.env.DBUSER || 'root',
  DBPASS: process.env.DBPASS || '',
  DBHOSTNAME: process.env.DBHOSTNAME || 'localhost',
  DBNAME: process.env.DBNAME || 'sprint_project_luis_lasso',
  REDIS_SERVER: process.env.REDIS_SERVER || 'localhost',
  REDIS_PASSWORD: process.env.REDIS_PASSWORD || '',
  REDIS_PORT: process.env.REDIS_PORT || 6379,
  APIVERSION: process.env.npm_package_version?.charAt(0) || 0,
  PROJECT_NAME: process.env.npm_package_name || 'api',
}

module.exports = ENV;