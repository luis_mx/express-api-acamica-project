const bunyan = require('bunyan');
const { PROJECT_NAME: name } = require('./env');

module.exports = bunyan.createLogger({ name });
