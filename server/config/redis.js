const { promisify } = require('util');
const redis = require('redis');

const log = require('../config/logger');
const { REDIS_SERVER: host, REDIS_PORT: port, REDIS_PASSWORD: password } = require('./env');

const redisClient = !!password
  ? redis.createClient({ host, port, password })
  : redis.createClient(`redis://${host}:${port}`);

redisClient.on('error', ({ message }) => log.fatal(`Redis ERR: ${message}`));

module.exports = {
  redisMGet: promisify(redisClient.mget).bind(redisClient),
  redisMSet: promisify(redisClient.mset).bind(redisClient),
  redisScan: promisify(redisClient.scan).bind(redisClient),
  redisUnlink: promisify(redisClient.unlink).bind(redisClient),
  redisFlushAll: promisify(redisClient.flushall).bind(redisClient),
};
