/**
 * @swagger
 *
 * tags:
 *  - name: Products
 *    description: Admins ONLY
 *  - name: Payments
 *    description: Admins ONLY
 *
 * components:
 *
 *  parameters:
 *
 *   userParam:
 *     name: id
 *     in: path
 *     description: ''
 *     required: true
 *     schema:
 *      type: string
 *      format: byte
 *      example: 6132e818d91dae30eecb2cbd
 *
 *   productParam:
 *     name: id
 *     in: path
 *     description: ''
 *     required: true
 *     schema:
 *      type: string
 *      format: byte
 *
 *   paymentParam:
 *     name: id
 *     in: path
 *     description: ''
 *     required: true
 *     schema:
 *      type: string
 *      format: byte
 *
 *   orderParam:
 *     name: id
 *     in: path
 *     description: ''
 *     required: true
 *     schema:
 *      type: string
 *      format: byte
 *
 *   userOrderParam:
 *     name: orderId
 *     in: path
 *     description: ''
 *     required: true
 *     schema:
 *      type: string
 *      format: byte
 *
 *   orderProductParam:
 *     name: productId
 *     in: path
 *     description: ''
 *     required: true
 *     schema:
 *      type: string
 *      format: byte
 *
 *   addressAliasParam:
 *     name: alias
 *     in: path
 *     description: ''
 *     required: true
 *     schema:
 *      type: string
 *
 *  responses:
 *
 *    ok:
 *      description: Status ok
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *             status:
 *              type: string
 *              example: ok
 *
 *    401:
 *      description: Incorrect authentication
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *             status:
 *              type: string
 *              example: error
 *             message:
 *              type: string
 *              example: Invalid credentials
 *
 *    404:
 *      description: Not found
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *             status:
 *               type: string
 *               example: 404
 *             message:
 *               type: string
 *               example: Not found.
 *
 *  schemas:
 *
 *    User:
 *      type: object
 *      required:
 *        - username
 *        - password
 *        - email
 *        - phone
 *        - address
 *        - isAdmin
 *        - id
 *      properties:
 *        username:
 *          type: string
 *          example: basicUser
 *        password:
 *          type: string
 *          format: password
 *          example: testondo
 *        fullName:
 *          type: string
 *          example: Basic User
 *        email:
 *          type: string
 *          example: basic_user@email.com
 *        phone:
 *          type: string
 *          example: '+523329485763'
 *        address:
 *          type: string
 *          example: 'Av Uno 65, int 701'
 *        orders:
 *          type: array
 *          items:
 *            $ref: '#/components/schemas/Order'
 *        isAdmin:
 *          type: boolean
 *          example: false
 *        id:
 *          type: integer
 *          format: int64
 *          example: 23
 *
 *    Order:
 *      type: object
 *      required:
 *        - date
 *        - status
 *        - products
 *        - userId
 *        - id
 *      properties:
 *        date:
 *          type: string
 *          format: date
 *        status:
 *          $ref: '#/components/schemas/OrderStatus'
 *          example: 'PENDING'
 *        total:
 *          type: integer
 *          example: 999
 *        paymentMethod:
 *          $ref: '#/components/schemas/Payment'
 *          example: CASH
 *        destination:
 *          type: string
 *          example: 'Av Siempre Viva 8474'
 *        products:
 *          type: array
 *          items:
 *            type: object
 *            properties:
 *              quantity:
 *                type: integer
 *                example: 2
 *              product:
 *                $ref: '#/components/schemas/Product'
 *        userId:
 *          type: integer
 *          format: int64
 *          example: 23
 *        id:
 *          type: integer
 *          format: int64
 *          example: 007
 *
 *    Product:
 *      type: object
 *      required:
 *        - name
 *        - price
 *        - id
 *      properties:
 *        name:
 *          type: string
 *          example: Bágel de salmón
 *        price:
 *          type: integer
 *          example: 425
 *        id:
 *          type: integer
 *          format: int64
 *          example: 99
 *
 *    Payment:
 *      type: object
 *      required:
 *        - type
 *        - name
 *        - id
 *      properties:
 *        type:
 *          type: string
 *          example: CASH
 *        name:
 *          type: string
 *          example: Efectivo
 *        id:
 *          type: integer
 *          format: int64
 *          example: 2
 *
 *    OrderStatus:
 *      type: string
 *      enum: [PENDING, CONFIRMED, PREPARATION, SEND, COMPLETED]
 *      example: COMPLETED
 */