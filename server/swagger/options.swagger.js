const { PORT: port, APIVERSION: vAPI } = require('../config/env');

exports.swaggerOptions = {
  definition: {
    openapi: '3.0.1',
    info: {
      title: 'Sprint Project',
      version: `v${vAPI}`,
      description: `<h3>Authorize API by:</h3>
      <ul>
        <li>Click on <strong>/login</strong> endpoint.</li>
        <li>From the dropdown, it's possible to choose between <strong>Admin</strong> and <strong>Regular</strong> user.</li>
        <li>Click <strong>Try it out</strong> and <strong>Execute</strong>.</li>
        <li>Response should bring back a <strong>Token</strong> string 🎟️.</li>
        <li>Click <strong>Authorize</strong> button and paste token.</li>
        <li>Lock should look close now 🔒.</li>
      </ul>
      `,
    },
    servers: [{ url: `http://127.0.0.1:${port}/v${vAPI}` }],
    components: {
      securitySchemes: {
        bearerAuth: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT',
        },
      },
    },
  },
  // swaggerUiOptions: {
  //   persistAuthorization: true,
  // },
  apis: ['./server/swagger/definitions.swagger.js', './routes/api/*.js'],
};
