const mongoose = require('mongoose');
const faker = require('faker');
const request = require('supertest');
const assert = require('assert');
const app = require('../app');
const { APIVERSION: APIv } = require('../server/config/env');

describe('Integration: /signup', () => {
  let response, userInfo, responseBody, adminToken;
  before(() => {
    request(app)
      .post(`/v${APIv}/login`)
      .send({ email: 'admin@email.com', password: 'admin' })
      .then((res) => {
        adminToken = res.body.token;
        done();
      });
  });
  before(() => {
    userInfo = {
      username: faker.internet.userName(),
      password: faker.internet.password(),
      email: faker.internet.email().toLowerCase(),
      fullName: `${faker.name.firstName()} ${faker.name.lastName()}`,
      phone: faker.phone.phoneNumber(),
      address: faker.address.streetAddress(),
    };
  });
  before((done) => {
    request(app)
      .post(`/v${APIv}/signup`)
      .set('Accept', 'application/json')
      .send(userInfo)
      .then((res) => {
        response = { ...res };
        responseBody = { ...res.body };
        done();
      })
      .catch((error) => done(error));
  });
  after((done) => {
    request(app)
      .del(`/v${APIv}/users/${responseBody.user._id}`)
      .auth(adminToken, { type: 'bearer' })
      .then((res) => done())
      .catch((error) => done(error));
  });

  it('status code 201', () => assert.strictEqual(response.status, 201));

  it('body-response should have same email values as body-request', () =>
    assert.strictEqual(responseBody.user.email, userInfo.email));

  it('body-response should have a token and be a string', () =>
    assert.strictEqual(typeof responseBody.token, 'string'));

  it('user should have one address in addressBook, marked as default', () => {
    assert.strictEqual(responseBody.user.addressBook.length, 1);
    assert.strictEqual(responseBody.user.addressBook[0].alias, 'default');
  });

  it('should return status error when email is duplicated', (done) => {
    request(app)
      .post(`/v${APIv}/signup`)
      .set('Accept', 'application/json')
      .send(userInfo)
      .expect(400)
      .then(({ body }) => assert.strictEqual(body.status, 'error') || done())
      .catch((error) => done(error));
  });
});
