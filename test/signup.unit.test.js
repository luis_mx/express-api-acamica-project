const mongoose = require('mongoose');
const chai = require('chai');
const sinon = require('sinon');
const faker = require('faker/locale/es_MX');
const expect = chai.expect;

const { User } = require('../models');
const { User: UserRepository } = require('../repository');

describe('Unit Test: /signup', () => {
  let user, userInfo, address;
  let stub, _user_;

  before(() => {
    userInfo = {
      _id: mongoose.Types.ObjectId(),
      username: faker.internet.userName(),
      password: faker.internet.password(),
      email: faker.internet.email(),
      fullName: `${faker.name.firstName} ${faker.name.lastName()}`,
      phone: faker.phone.phoneNumber(),
    };

    address = faker.address.streetAddress();

    user = new User(userInfo);
    user.addressBook.push({ destination: address, alias: 'default' });
  });

  it('Should add new User to database', async () => {
    stub = sinon.stub(User.prototype, 'save').returns(userInfo);
    const response = await UserRepository.create(userInfo, address);
    _user_ = response.user;

    expect(stub.calledOnce).to.be.true;
  });
  it('user & stub should have the same ID', () =>
    expect(_user_._id).to.equal(user._id.toString())
  );
  it('user & stub should have the same email', () =>
    expect(_user_.email).to.equal(user.email)
  );
  it('user & stub should have the same default address', () =>
    expect(_user_.addressBook.default).to.equal(user.addressBook.default)
  );
});
