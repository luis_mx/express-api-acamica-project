module.exports = (res, next, statusCode) => {
  return async (fn, ...args) => {
    try {
      const data = args ? await fn(...args) : await fn();
      return statusCode ? res.status(statusCode).send(data) : res.send(data);
    } catch (error) {
      next(error);
    }
  };
}
