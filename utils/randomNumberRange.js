const log = require('../server/config/logger');

module.exports = (a, b) => {
  if (b === undefined) return log.warn(`need two integers`);
  const min = a > b ? b : a;
  const max = b > a ? b : a;
  return Math.floor(Math.random() * (max - min)) + min;
};