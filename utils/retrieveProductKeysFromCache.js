const { redisScan } = require('../server/config/redis');

module.exports = async () => {
  try {
    const productKeys = [];
    let products = [];
    let cursor = '0';
    do {
      const response = await redisScan(cursor, 'MATCH', 'product:*', 'COUNT', '100');
      [cursor, products] = response;
      productKeys.push(products);
    } while (cursor !== '0');

    return productKeys.flat();

  } catch (error) {
    throw error;
  }
};
