const deleteNotStringKeys = (obj) => {
  Object.keys(obj).forEach((key) => typeof obj[key] !== 'string' && delete obj[key]);
  Object.keys(obj).forEach((key) => obj[key] === '' && delete obj[key]);
  return obj;
};

const deleteNotStringNotNumberKeys = (obj) => {
  Object.keys(obj).forEach(
    (key) => typeof obj[key] !== 'string' && typeof obj[key] !== 'number' && delete obj[key]
  );
  Object.keys(obj).forEach(
    (key) => typeof obj[key] === 'string' && obj[key] === '' && delete obj[key]
  );
  return obj;
};

const deleteEmptyStringKeys = (obj) => {
  Object.keys(obj).forEach((key) => obj[key] === '' && delete obj[key]);
  return obj;
};

module.exports = {
  deleteNotStringKeys,
  deleteNotStringNotNumberKeys,
  deleteEmptyStringKeys,
};
