const { redisMSet } = require('../server/config/redis');

module.exports = async (dataObjArr, keyname) =>
  await redisMSet(
    dataObjArr
      .map((dataObj) => [`${keyname}:${dataObj._id.toString()}`, JSON.stringify(dataObj)])
      .flat()
  );
