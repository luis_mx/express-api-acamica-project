module.exports = obj =>
  Object.entries(obj)
    .flat()
    .map(value => value.toString());
