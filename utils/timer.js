const { hrtime } = require('process');

const log = require('../server/config/logger');

class Timer {
  constructor(message = 'Timer end') {
    this.startTimer = 0;
    this.endTimer = 0;
    this.message = message;
  }

  start() {
    this.startTimer = hrtime.bigint();
  }

  end() {
    this.endTimer = hrtime.bigint();

    log.info(`${this.message}, ${(Number(this.endTimer - this.startTimer) / 1e9).toFixed(3)}ms`);

    this.startTimer = 0;
    this.endTimer = 0;
  }
}

module.exports = Timer;
